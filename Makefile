# ubuntu-rust-test-c

all: help

.PHONY: all help build run

help:
	@echo "Available targets:"
	@echo
	@echo "  build      build and tag Docker image"
	@echo "  run        run Docker image"
	@echo


CUSTOM_VERSION?=0.1
UBUNTU_VERSION?=18.04
RUST_VERSION?=1.38.0

DOCKER_IMAGE:=ubuntu-rust-test-c
DOCKER_REGISTRY_URL?=registry.gitlab.com/docking/
DOCKER_TAG?=$(CUSTOM_VERSION)-$(UBUNTU_VERSION)-$(RUST_VERSION)

build:
	docker build \
		--build-arg DOCKER_REGISTRY_URL=$(DOCKER_REGISTRY_URL) \
		--build-arg CUSTOM_VERSION=$(CUSTOM_VERSION) \
		--build-arg UBUNTU_VERSION=$(UBUNTU_VERSION) \
		--build-arg RUST_VERSION=$(RUST_VERSION) \
		--tag $(DOCKER_IMAGE):$(DOCKER_TAG) \
		$(if $(DOCKER_NO_CACHE),--no-cache,) \
		.

run:
	docker run \
		$(DOCKER_IMAGE):$(DOCKER_TAG)
